const puppeteer = require('puppeteer');
const fs = require('fs');

 
(async () => {
  const certificate = fs.readFileSync("temp.html").toString()  
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  // await page.setViewport({width: 794, height: 1122, deviceScaleFactor: 2});
// await page.goto(`https://www.example.org`, {waitUntil: 'networkidle'});

  await page.setViewport({ width: 1440, height: 900 , deviceScaleFactor: 3});
  await page.setContent(certificate);
  await page.pdf({path: 'template17.pdf', format: 'A4' });
 
  await browser.close();
})();